/*
 * Copyright (C) 2021 xuexiangjys(xuexiangjys@163.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.xuexiang.templateproject.fragment.test;

import com.xuexiang.templateproject.R;

import com.xuexiang.templateproject.core.BaseFragment;
import com.xuexiang.templateproject.fragment.other.AboutFragment;
import com.xuexiang.templateproject.fragment.other.SettingsFragment;
import com.xuexiang.xui.widget.actionbar.TitleBar;
import com.xuexiang.xui.widget.button.ButtonView;
import com.xuexiang.xui.widget.textview.supertextview.SuperTextView;
import com.xuexiang.xui.widget.textview.supertextview.SuperButton;

import com.xuexiang.xaop.annotation.SingleClick;
import com.xuexiang.xpage.annotation.Page;
import com.xuexiang.xpage.enums.CoreAnim;
import com.xuexiang.templateproject.utils.XToastUtils;
import com.xuexiang.xutil.app.ActivityUtils;
import com.xuexiang.templateproject.activity.MainActivity;

import butterknife.BindView;
import butterknife.OnClick;

import android.view.View;

/**
 * Created by lyg on 2021-07-30.
 */
@Page(anim = CoreAnim.none)
public class TestFragment extends BaseFragment implements SuperTextView.OnSuperTextViewClickListener {
    @BindView(R.id.clickBtn)
    ButtonView clickBtn;
    @BindView(R.id.click_btn)
    SuperTextView clickFn;
    @BindView(R.id.icon_mark)
    SuperButton mark;

    /**
     * @return 返回为 null意为不需要导航栏
     */
    @Override
    protected TitleBar initTitle() {
        return null;
    }

    /**
     * 布局的资源id
     *
     * @return
     */
    @Override
    protected int getLayoutId() {
        return R.layout.fragment_test;
    }

    /**
     * 初始化控件
     */
    @Override
    protected void initViews() {
        //        clickBtn.setOnClickListener(this);
    }

    @Override
    protected void initListeners() {
        clickFn.setOnSuperTextViewClickListener(this);
        //        clickBtn.setOnClickListener();

    }

    /**
     * 按钮的点击事件 - 跳转 Activity
     * ButtonView
     * @param view
     */
    @SingleClick
    @OnClick(R.id.clickBtn)
    public void onViewClicked(View view) {
        XToastUtils.toast("点击了:=====clickBtn=========");
        ActivityUtils.startActivity(MainActivity.class);
    }

    /**
     *  按钮的点击事件 - 跳转 Fragment
     * ButtonView
     * @param view
     */
    @OnClick(R.id.clickBtn1)
    public void onViewClicked1(View view) {
        XToastUtils.toast("点击了:=====clickBtn1=========");
        openNewPage(AboutFragment.class);
    }

    /**
     *
     * @param view
     */
    @OnClick(R.id.icon_mark)
    public void onViewClicked2(View view) {
        XToastUtils.toast("点击了:=======icon_mark=======");
        openNewPage(AboutFragment.class);
    }

    @OnClick(R.id.confirm_button)
    public void onViewClicked3(View view) {
        XToastUtils.toast("点击了:=======confirm_button=======");
        openNewPage(AboutFragment.class);
    }


    @SingleClick
    @Override
    public void onClick(SuperTextView view) {
        switch (view.getId()) {
            case R.id.click_btn:
                XToastUtils.toast("点击了:===========SuperTextView========" + view.getId());
                //                openNewPage(AboutFragment.class);
                break;
            default:
                break;
        }
    }
}